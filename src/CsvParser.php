<?php
declare(strict_types=1);

namespace Shifft\CsvParser;

/**
 *
 * Class CsvParser
 * @package Shifft\CsvParser
 */
class CsvParser
{
	//The possible error number values
	const ERROR_NOINPUT		 = 0;
	const ERROR_NODELIMITER	 = 1;
	
	private $data = '';
	public $delimiter = '';
	private $error = '';
	private $errorNumb = -1;
	private $lines = [];
	private $headers = [];
	private $delimiters = array(
        ';'	 => 0,
        ','	 => 0,
        "\t" => 0,
        "|"	 => 0
    );
	
	/**
	 *  @brief returns a property of the class
	 *  
	 *  @param string $name the name of the property to return
	 *  @return mixed returns the value of the property
	 */
	public function __get(string $name)
	{
		if(property_exists($this, $name))
		{
			return $this->$name;
		}
	}

	/**
	 *  @brief sets the delimiter
	 *  
	 *  @param string $delimiter the vaulue to wich the delimiter needs to be set
	 *  @return void
	 */
	public function setDelimiter(string $delimiter)
	{
		$this->delimeter = $delimiter;
	}

	
	/**
	 *  @brief creates a CsvParser object.
	 *  
	 *  @param string $input the input containing a path to a csv, a csv string or NULL. If not null the input will be parsed 
	 *  @return CsvParser, the CsvParser object thats created
	 */
	public function __construct(string $input = null)
	{
		if($input != null)
		{
			$this->parse($input);
		}
	}
	
	/**
	 *  @brief Parses the supplied csv input
	 *  
	 *  @param string $input the path or csv string to parse
	 *  @return return true if succesfull otherwise it returns false, check the error and errorNumb to show whats wrong.
	 */
	public function parse(string $input):bool
	{
		if (!empty($input))
		{
			$this->data = (strlen($input) <= PHP_MAXPATHLEN && is_readable($input))  ? file_get_contents($input) : $input;
			$this->lines = preg_split('/\r\n|\r|\n/', $this->data);
			
			if(!isset($this->delimiter) || empty($this->delimiter)){
				$this->detectDelimiter();

			}
			if(empty($this->delimiter))
			{
				if($this->errorNumb != -1)
				{
					$this->error = 'No delimter found.';
					$this->errorNumb = self::ERROR_NODELIMITER;
				}
				return false;
			}
			
			for($i = 0; $i < count($this->lines); $i++)
			{
				$this->lines[$i] = explode($this->delimiter, $this->lines[$i]);
			}
			$this->getHeaders();
			
			return true;
		}
		$this->errorNumb = self::ERROR_NOINPUT;
		$this->error = 'Empty input detected.';
		return false;
	}
	
	/**
	 *  @brief This function will detected the delimiter of the csv
	 *  
	 *  @return void
	 */
	private function detectDelimiter(): void
	{
		if(count($this->lines) == 0)
		{
			$this->errorNumb = self::ERROR_NOINPUT;
			$this->error = 'Empty input detected.';
			return;
		}

		do
		{
			foreach ($this->delimiters as $delimiter => &$count) 
			{
				$count = count(str_getcsv($this->lines[0], $delimiter));
			}
			$delimiter = array_search(max($this->delimiters), $this->delimiters);
			$count = $this->delimiters[$delimiter];
			$correct = true;
			for($i = 1; $i < count($this->lines); $i++)
			{
				if(!empty($this->lines[$i]))
				{
					if($count != count(str_getcsv($this->lines[$i], $delimiter)))
					{
						$correct = false;
					}
				}
				else
				{
					unset($this->lines[$i]);
					$i--;
				}
			}
			if($correct)
			{
				$this->delimiter = $delimiter;
			}
			else
			{
				unset($this->delimiters[$delimiter]);
			}
		}while(empty($this->delimiter) && count($this->delimiters) > 0);

		if(count($this->lines) == 0)
		{
			$this->errorNumb = self::ERROR_NOINPUT;
			$this->error = 'Empty input detected.';
			return;
		}
	}
	
	/**
	 *  @brief This function will detirmine if the csv has headers and if it has them, store them in the headers property
	 *  
	 *  @return void
	 */
	private function getHeaders(): void
	{
		if(count($this->lines) == 0)
		{
			$this->errorNumb = self::ERROR_NOINPUT;
			$this->error = 'Empty input detected.';
			return;
		}
		$hasHeader = true;
		foreach($this->lines[0] as $i=>$column)
		{
			if(filter_var($column, FILTER_VALIDATE_EMAIL) !== false || filter_var($column, FILTER_VALIDATE_URL) !== false || filter_var($column, FILTER_VALIDATE_INT) !== false || filter_var($column, FILTER_VALIDATE_FLOAT) !== false)
			{
				$hasHeader = false;
				break;
			}
		}
		if($hasHeader)
		{
			$this->headers = $this->lines[0];
			unset($this->lines[0]);
			$this->lines = array_values($this->lines);
		}
	}
}
?>