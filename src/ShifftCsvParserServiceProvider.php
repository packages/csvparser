<?php

namespace Shifft\CsvParser;

use Illuminate\Support\ServiceProvider;

class ShifftCsvParserServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(){
	}

    /**
     * Register services.
     *
     * @return void
     */
    public function register(){
        // Make sure the facade can be used by just having "use CsvParser;" in files.
        $this->app->singleton('CsvParser', function($app) {
            return new CsvParser();
        });
    }
}
