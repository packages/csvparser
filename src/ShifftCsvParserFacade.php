<?php
namespace Shifft\CsvParser;

use Illuminate\Support\Facades\Facade;

/**
 * Do not change anything here.
 *
 * Class ErrorHandlingFacade
 * @package App\Services\ErrorHandling
 */
class ShifftCsvParserFacade extends Facade
{
    /**
     * Register Facade Accessor
     * @return string
     */
    protected static function getFacadeAccessor() {
        return 'CsvParser';
    }
}
