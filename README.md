## CsvParser
#### What is this?
A simple csv parser, with automatic delimiter detection and header detection

#### Getting Started
To get started, require the package with `composer require shifft/csvparser`

#### Usage
Have the following code at the top of your file
```php
use Shifft\CsvParser\CsvParser;
```

And use it as such:
```php
$parser = new CsvParser();
$parser->parse($data);//data can be a path to a csv file or a csv string
$data = $parser->lines;//get the csv data
$headers = $parser->headers;//get the csv headers
```

